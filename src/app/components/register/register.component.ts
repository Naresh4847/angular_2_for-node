import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Http, Headers, Request, Response, RequestOptions } from '@angular/http';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

// class FileSelectDirective

export class RegisterComponent implements OnInit {
  name: String;
  username: String;
  email: String;
  password: String;
  confirmPassword: string;

  constructor(
    private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password,
      confirmpassword: this.confirmPassword
    }
    console.log(user);

    // Required Fields
    if (!this.validateService.validateRegister(user)) {
      this.flashMessage.show('Please Fill All Fields', { cssClass: 'flash-message', timeout: 3000 });
      return false;
    }
    if(this.password !== this.confirmPassword){
      this.flashMessage.show('password is not matched ', { cssClass: 'flash-message', timeout: 3000 });
      return false;
    }else{
      this.router.navigate(['/login']);
    }
    // validate Email
    if (!this.validateService.validateEmail(this.email)) {
      this.flashMessage.show('Please Enter Valid Email', { cssClass: 'flash-message', timeout: 3000 });
      return false;
    }

    //Register User 
    this.authService.registerUser(user).subscribe(data => {
      if (data) {
        this.flashMessage.show('You are registered', { cssClass: 'flash-message', timeout: 2000 });
        this.router.navigate(['/login']);
      } else {
        this.flashMessage.show('You are SomethingWrong', { cssClass: 'flash-message', timeout: 2000 });
        this.router.navigate(['/register']);
      }
    });

  }
   
}

