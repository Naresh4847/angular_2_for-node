import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Profile } from 'selenium-webdriver/firefox';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';

import { Http, Response } from '@angular/http';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';

const URL = 'http://localhost:3000/profileimg';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']

})

export class ProfileComponent implements OnInit {

  name: String;
  username: String;
  email: String;
  password: String;
  confirmPassword: string;
  user;

  public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'image' });

  constructor(private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router,
    private http: Http, private el: ElementRef) { }

  ngOnInit() {
    this.authService.getProfile().subscribe(Profile => {
      this.user = Profile.user;
      this.name = this.user.name,
        this.username = this.user.username,
        this.email = this.user.email
    });
    err => {
      console.log(err);
      return false;
    }
  }

  uploadfile = () => {
    alert('mmsbmfmdbmfsm');
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log("ImageUpload:uploaded:", item, status, response);
    };
   }
  

  updateSubmit() {
    this.user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password,
      confirmpassword: this.confirmPassword
    }
    console.log(this.user);
    //Register User 
    this.authService.updateUser(this.user).subscribe(data => {
      console.log('response from profile' + data);
      if (data) {
        this.flashMessage.show('You are Profile Updated', { cssClass: 'flash-message', timeout: 2000 });
        this.router.navigate(['/dashboard']);
      } else {
        this.flashMessage.show('You are SomethingWrong', { cssClass: 'flash-message', timeout: 2000 });
        this.router.navigate(['/profile']);
      }
    });
  }
}